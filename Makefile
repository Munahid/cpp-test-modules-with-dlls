#

clean:
	rm -rf gcm.cache *.o

mod:
	g++ -std=c++17 -fmodules-ts -xc++-system-header iostream
	g++ -std=c++17 -fmodules-ts -xc++-system-header string

# check if import iostream works
1:
	g++ -std=c++17 -fmodules-ts main1.cpp -o main1.exe

# simple module check
2:
	g++ -std=c++17 -fmodules-ts -c -x c++ mod.cpp -o mod.o
	g++ -std=c++17 -fmodules-ts -c -x c++ modimpl.cpp -o modimpl.o
	g++ -std=c++17 -fmodules-ts main2.cpp -o main2.exe modimpl.o mod.o

# make a lib out of the module
lib:
	g++ -std=c++17 -fmodules-ts -c -x c++ mod.cpp -o mod.o
	g++ -std=c++17 -fmodules-ts -c -x c++ modimpl.cpp -o modimpl.o
	g++ -std=c++17 -fmodules-ts -shared -Wl,--out-implib,module.dll.a -Wl,--subsystem,windows -o module.dll mod.o modimpl.o

# use the lib and the module interface, without implementation sources
3: mod
	g++ -std=c++17 -fmodules-ts -c -x c++ mod.cpp -o mod.o
	g++ -std=c++17 -fmodules-ts main3.cpp -o main3.exe -L. -l:module.dll.a

# a new module which uses the module from the lib
4: mod
	g++ -std=c++17 -fmodules-ts -c -x c++ mod.cpp -o mod.o
	g++ -std=c++17 -fmodules-ts -c -x c++ modx.cpp -o modx.o
	g++ -std=c++17 -fmodules-ts -c -x c++ modximpl.cpp -o modximpl.o
	g++ -std=c++17 -fmodules-ts main4.cpp -o main4.exe modximpl.o modx.o -L. -l:module.dll.a

all: mod
